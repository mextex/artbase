#!/usr/bin/env python
# xmp.py


# Import Librariess
import pyexiv2
import json
import os.path
from libxmp import XMPFiles


class Artbase():

    def __init__(self):
        self.xmp_scheme = json.load(open('artbase_xmp.json', 'r'))
        self.xmp_scheme_arrays = []
        self.xmp_scheme_properties = []
        for _property in self.xmp_scheme:
            if self.property_has_key(_property, "arrayType"):
                self.xmp_scheme_arrays.append(_property["propertyName"])
            elif self.property_has_key(_property, "propertyName"):
                self.xmp_scheme_properties.append(_property["propertyName"])
        self.namespace = "http://www.peterwendl.de/artwork/"
        self.prefix = "artwork"

    def property_has_key(self, _property, _key):
        try:
            _property[_key]
        except KeyError:
            return False
        return True

    def set_xmp(self, _file_path, **properties):
        metadata = pyexiv2.ImageMetadata(_file_path)
        metadata.read()
        try:
            pyexiv2.xmp.register_namespace(self.namespace, self.prefix)
        except Exception:
            pass
        base = "Xmp." + self.prefix + "." + "foo"
        metadata[base] = "bar"
        metadata.write()
        xmpfile = XMPFiles(file_path=_file_path, open_forupdate=True)
        xmp = xmpfile.get_xmp()
        for key, value in properties.items():
            if value != "-":
                if key in self.xmp_scheme_arrays:
                    value_items = value.split(",")
                    for value_item in value_items:
                        # if key == "tags":
                        #     current_namespace = "http://purl.org/dc/elements/1.1/"
                        #     current_key = "subject"
                        # else:
                        #     current_namespace = self.namespace
                        #     current_key = key
                        print(key)
                        if not xmp.does_array_item_exist(self.namespace, key, value_item):
                            xmp.append_array_item(self.namespace, u'' + key + '', u'' + value_item + '', {'prop_array_is_ordered': True, 'prop_value_is_array': True})
                elif key in self.xmp_scheme_properties:
                    xmp.set_property(self.namespace, u'' + key + '', u'' + value + '')
                else:
                    print("Key '" + key + "' is not in current xmp scheme")
        try:
            xmpfile.put_xmp(xmp)
        except Exception:
            print("Could not update xmp information")
        xmpfile.close_file()

    def delete_property(self, _xmp, _property):
        _xmp.delete_property(self.namespace, _property)

    def read_import_file(self):
        database_filename = "test_data"
        with open(database_filename, encoding='utf8') as f:
            database = f.read()
            assets = database.split("\n")
            print(assets)
            for asset in assets:
                properties = asset.split("\t")
                path = properties[0]
                path_parts = path.split(":")
                path_parts.pop(0)
                path_new = "/" + "/".join(path_parts)
                print("File: " + path_new)
                if self.file_exists(path_new):
                    title = properties[2]
                    artist = properties[3]
                    genre = properties[4]
                    year = properties[5]
                    tags = properties[6]
                    description = properties[7]
                    collections = properties[8]
                    location = properties[10]
                    city = properties[11]
                    epoch = properties[12]
                    try:
                        self.set_xmp(path_new, title=title, artist=artist, genre=genre, year=year, description=description, location=location, city=city, epoch=epoch, collections=collections, tags=tags)
                    except Exception:
                        print(Exception)

    def file_exists(self, file):
        if os.path.isfile(file):
            print("yes")
            return True
        else:
            print("no")
            return False


artbase = Artbase()
# file_path = "/Users/peterwendl/Pictures/Bildarchiv Extern/Design/Designer/Neurath/Neurath_Atlas_Gesellschaft_u_Wirtschaft_1931 1.pdf"
# artbase.set_xmp(file_path, artist="Marce", title="tdftt", tags="Skulptur,Maler")
artbase.read_import_file()


# Asset Data Scheme:
# Pfadname    Dateiname   Titel   Erstellt von    Gattung Jahr    SchlŸsselwšrter Beschreibung    KatalogsŠtze    Standort    Stadt   Land/Region Epoche
